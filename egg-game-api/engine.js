var redis = require('./redis');

module.exports.current_clicks = null;
module.exports.settings = {};

const preset_admin_user = 'admin';
const preset_admin_pass = 'admin';
const preset_initial_clicks = 5000;

module.exports.INITIAL_CLICKS = 'initial_clicks';
module.exports.ADMIN_USER = 'admin_user';
module.exports.ADMIN_PASS = 'admin_pass';
module.exports.CLICKS = 'clicks';

module.exports.changeSetting = function(key, val) {
    module.exports.settings[key] = val;
    if(key === "clicks") {
        module.exports.current_clicks = val;
    }
    redis.changeValue('egg-game:' + key, val);
}

module.exports.click = function() {
    if(module.exports.current_clicks > 0) {
        redis.click();
        module.exports.current_clicks--;
    }
}
module.exports.login = (socket, user, pass) => {
    if(user === module.exports.settings.admin_user) {
        if (pass === module.exports.settings.admin_pass) {
            socket.admin = true;
            return true;
        }
    }
    return false;
}

module.exports.initialize = function() {
    redis.getSettings().then(settings => {
        if(!settings.admin_user){
            module.exports.changeSetting(module.exports.ADMIN_USER, preset_admin_user);
            settings.admin_user = preset_admin_user;
            console.log('defaulted admin login to "' + settings.admin_user + '"')
        }
        if(!settings.admin_pass) {
            module.exports.changeSetting(module.exports.ADMIN_PASS, preset_admin_pass);
            settings.admin_pass = preset_admin_pass;
            console.log('defaulted admin pass to "' + settings.admin_user + '"')
        }
        if(!settings.initial_clicks) {
            module.exports.changeSetting(module.exports.INITIAL_CLICKS, preset_initial_clicks);
            settings.initial_clicks = preset_initial_clicks;
            console.log('defaulted initial clicks value to', settings.initial_clicks);
        }
        if(!settings.clicks) {
            module.exports.changeSetting(module.exports.CLICKS, settings.initial_clicks);
            settings.clicks = settings.initial_clicks;
            console.log('defaulted clicks value to', settings.clicks);
        }
        module.exports.current_clicks = settings.clicks;
        module.exports.settings = settings;
    });
}