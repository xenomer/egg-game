var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var tools = require('./redis');
var engine = require('./engine');
var randomstring = require('randomstring');
var logincache = { }

io.on('connection', function(socket){
    socket.on('click', (msg) => {
        engine.click();
        io.emit('count update', engine.current_clicks);
    });
    socket.on('admin login', (usr, pass, cb) => {
        if(socket.admin) return;
        if(logincache.hasOwnProperty(usr)) {
            socket.admin = true;
            socket.loginId = usr;
        }
        else if(engine.login(socket, usr, pass)){
            var id = randomstring.generate(7);
            console.log("new adming logged in (id", id + ')');
            logincache[id] = {id: id, user: usr};
            socket.loginId = id;
        }
        if(socket.loginId) {
            var set = engine.settings;
            set.clicks = engine.current_clicks;
            socket.emit('admin update', set);
        }
        cb(socket.loginId);
    })
    socket.on('admin save', (set) => {
        if(socket.admin) {
            for (var key in set) {
                if(set.hasOwnProperty(key)){
                    var setting = null;
                    switch(key){
                        case engine.ADMIN_USER:
                        case engine.ADMIN_PASS:
                        case engine.CLICKS:
                        case engine.INITIAL_CLICKS:
                            setting = key;
                            break;
                    }
                    if(setting) {
                        engine.changeSetting(setting, set[key]);
                    }
                }
            }
            var set = engine.settings;
            set.clicks = engine.current_clicks;
            io.emit('count update', set.clicks);
            socket.emit('admin update', set);
        }
    });
    socket.emit('count update', engine.current_clicks);
});

app.get('/', function (req, res) {
    
});
app.get('/click', function (req, res) {
    
})

//on redis ready
tools.onReady(() => {
    console.log('connected to redis database');
    engine.initialize();
});

var port = process.env.PORT || 3001;
http.listen(port, function(){
  console.log('listening on *:' + port);
});