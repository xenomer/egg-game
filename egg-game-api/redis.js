var redis = require("redis"),
    redisClient = redis.createClient(process.env.REDISADDR);

const {promisify} = require('util');
const getAsync = promisify(redisClient.get).bind(redisClient);

redisClient.on("error", function (err) {
    console.log("Redis error " + err);
});

module.exports.onReady = (cb) => {
    redisClient.on('ready', cb);
} 

module.exports.getCount = (cb) => {
    redisClient.get("egg-game:clicks", (err, count) => {
        if(err)
            cb(err);
        else cb(count);
    });
}

module.exports.click = () => {
    redisClient.decr("egg-game:clicks", (err, msg) => {
        if(err)
            console.log(err);
    });
}
module.exports.changeValue = function(key, value) {
    redisClient.set(key, value);
}
module.exports.getSettings = async function() {
    var settings = {};
    settings.clicks = await getAsync('egg-game:clicks');
    settings.admin_user = await getAsync('egg-game:admin_user');
    settings.admin_pass = await getAsync('egg-game:admin_pass');
    settings.initial_clicks = await getAsync('egg-game:initial_clicks');
    return settings;
}