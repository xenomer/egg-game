import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from "react-router-dom";
import './App.css';
import * as api from './api.js';
import Admin from './admin';
import Game from './game';
import posed, { PoseGroup } from 'react-pose';
import * as queryString from 'query-string';

const RoutesContainer = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 }
});

class App extends Component {
  constructor(props) {
    super(props);
    var query = queryString.parse(props.location.search);
    this.state = {
      count: null,
      status: 'disconnected',
      admin: query.admin,
      click_cache: []
    }
  }
  componentDidMount() {
    api.getSocket().on('disconnect', () => {
      this.setState({status: "disconnected", count: null})
    });
    api.getSocket().on('connect', () => {
      this.setState({status: "connected"})
    });
    api.onClickChanged((c) => {
      this.setState({count: c});
    });
  }
  render() {
    if(this.state.status === "disconnected") {
      return (<div className="App"><header className="App-header"><p>Connection Error</p></header></div>)
    }

    if(this.state.admin) {
      return (
        <Router>
          <div className="App">
            <header className="App-header">
              <Route render={(p) => {return (
                <Admin 
                  {...p}
                  />)}} />
            </header>
          </div>
        </Router>
      )
    }

    //eslint-disable-next-line
    var loc = location;
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <Route render={(p) => { return (<Game {...p} count={this.state.count} />) }} />
          </header>
        </div>
      </Router>
    );
  }
}

export default App;
