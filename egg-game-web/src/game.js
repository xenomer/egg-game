import React from 'react';
import * as api from './api.js';
import 'animate.css';
import posed from 'react-pose';
import { Player, ControlBar } from 'video-react';

const ClickerBox = posed.input({
    pressable: true,
    init: { 
        scale: 1,
        transition: { duration: 100 },
    },
    press: { 
        scale: 0.96,
        transition: { duration: 100 },
    }
});

export default class Game extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        document.addEventListener("contextmenu", function(e) {
            e.preventDefault();
        }, false);
        if(this.props.count === 0 || this.props.count < 0) {
            return (
                <div className="App">
                    <header className="App-header">
                        <Player 
                            playsInline
                            autoPlay
                            loop
                            src="wow.mp4">
                            <ControlBar disableCompletely />
                        </Player>
                        <p className="animated infinite pulse complete">WOW!</p>
                    </header>
                </div>
            )
        }
        else {
            return (
                <div className="App">
                    <p className="smalltext">Saaran 17v munaklikkisynttäripeli!! Klikkaa loppuun asti niin löydät yllätyksen!</p>
                    <header className="App-header">
                        <p className="headertext">{this.props.count}</p>
                        <ClickerBox type="image" src="/saara17/egg.png" touchstart={() => {api.click()}} className="clicker" onMouseDown={() => {api.click()}} value="KLIKKAA" />
                    </header>
                </div>
            )
        }
    }
}