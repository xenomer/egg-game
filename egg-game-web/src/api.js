import openSocket from 'socket.io-client';
const socket = openSocket('http://www.xenomer.me', {
    'reconnect': true,
    path: '/api/egggame/socket.io'
});
export var count = 0;
var cache = [];
var listeners = [];
socket.on("count update", (c) => {
    if(cache.includes(c)){
        cache.splice(cache.indexOf(c), 1);
    }
    else {
        count = c;
        listeners.forEach(cb => cb(c));
    }
});

export function click() {
    count--;
    cache.push(count);
    listeners.forEach(cb => cb(count));
    socket.emit("click");
}
export function getCount() {
    return count;
}
export function getSocket() {
    return socket;
}
export function onClickChanged(cb) {
    listeners.push(cb);
}
export function login(usr, pass, cb) {
    socket.emit('admin login', usr, pass, cb);
}