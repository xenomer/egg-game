import React from 'react';
import * as api from './api.js';
import * as queryString from 'query-string';

export default class Admin extends React.Component {
    constructor(props) {
        super(props);
        var query = queryString.parse(props.location.search);
        this.state = {
            loggedIn: false,
            props: props,
            form_user: '',
            form_pass: '',
            login_id: query.loginid,
        }

        this.onFormChange = this.onFormChange.bind(this);
        this.login = this.login.bind(this);
        this.onSettingsChange = this.onSettingsChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onFormChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    login = (e) => {
        if (e) e.preventDefault();
        api.login(this.state.form_user, this.state.form_pass, (loginid) => {
            if(loginid) this.props.history.push('/?admin=true?loginid=' + loginid);
            this.setState({loggedIn: loginid != null, login_id: loginid});
        });
    }
    onSubmit(e) {
        if (e) e.preventDefault();
        api.getSocket().emit('admin save', this.state.settings);
    }
    onSettingsChange(e) {
        var settings = {...this.state.settings};
        settings[e.target.name] = e.target.value;
        this.setState({
            settings: settings
        })
    }
    componentDidMount() {
        if(this.state.login_id) {
            this.state.form_user = this.state.login_id;
            this.login(null);
        }
        api.getSocket().on('admin update', (set) => {
            this.setState({settings: set});
        })
        api.getSocket().on('count update', (c) => {
            if(this.state.settings){
                var settings = this.state.settings;
            settings.clicks = c;
            this.setState({settings: settings}); 
            }
        });
    }
    render() {
        if(!this.state.loggedIn) {  
            return (<form onSubmit={this.login}>
                <input type="text" placeholder="Username" name="form_user" onChange={this.onFormChange} /> <br />
                <input type="password" placeholder="Password" name="form_pass" onChange={this.onFormChange} /> <br />
                <input type="submit" value="login" />
            </form>)
        }
        else {
            return (
                <form onSubmit={this.onSubmit}>
                    <input type="number" placeholder="clicks" name="clicks" value={this.state.settings.clicks} onChange={this.onSettingsChange} /> <br />
                    <input type="number" placeholder="initial clicks" name="initial_clicks" value={this.state.settings.initial_clicks} onChange={this.onSettingsChange} /><br />
                    <input type="text" placeholder="admin username" name="admin_user" value={this.state.settings.admin_user} onChange={this.onSettingsChange} /><br />
                    <input type="password" placeholder="admin pass" name="admin_pass" value={this.state.settings.admin_pass} onChange={this.onSettingsChange} /><br />
                    <input type="submit" value="apply" />
                </form>
            )
        }
    }
}